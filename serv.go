package main

import (
    "html/template"
    "io"
    "log"
    "net/http"
    "os"
    "path"
    "sort"
)

func main() {
    if len(os.Args) > 1 {
        if err := os.Chdir(os.Args[1]); err != nil {
            log.Fatal(err)
        }
        log.Println("Changed directory to", os.Args[1])
    }

    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        log.Println(r.Method, r.URL, r.Proto, r.RemoteAddr)

        p := path.Join(".", path.Join("/", r.URL.Path))
        switch r.Method {
        case "GET":  serveFile(w, r, p)
        case "POST": handleUpload(w, r, p)
        default:     http.Error(w, "405 Method Not Allowed.", 405)
        }
    })
    log.Println("Listening on :3000")
    http.ListenAndServe(":3000", nil)
}

func handleUpload(w http.ResponseWriter, r *http.Request, p string) {
    err := r.ParseMultipartForm(20 << 20)
 	if err != nil {
		log.Println(err)
        http.Error(w, "500 Internal Error: ParseMultipartForm() failed.", 500)
 		return
 	}
    for _, formFile := range r.MultipartForm.File["files"] {
        name := path.Join(p, path.Base(path.Join("/", formFile.Filename)))

        _, err := os.Stat(name)
        if err == nil || !os.IsNotExist(err) {
            http.Error(w, "409 Conflict: File already exists.", 409)
            return
        }

        out, err := os.Create(name)
        if err != nil {
            log.Println(err)
            http.Error(w, "500 Internal Error: Create() failed.", 500)
            return
        }
        defer out.Close()

        in, err := formFile.Open()
        if err != nil {
            log.Println(err)
            http.Error(w, "500 Internal Error: Open() failed.", 500)
            return
        }
        defer in.Close()

        _, err = io.Copy(out, in)
        if err != nil {
            log.Println(err)
            http.Error(w, "500 Internal Error: Copy() failed.", 500)
            return
        }

        log.Printf("Uploaded file: %s\n", name)
    }
    http.Redirect(w, r, path.Join("/", p), http.StatusSeeOther)
}

func serveFile(w http.ResponseWriter, r *http.Request, p string) {
	w.Header().Set("Server", "Serv/0.1")
	f, err := os.Open(p)
	if err != nil {
        http.Error(w, "404 Not Found: File not found.", 404)
		return
	}
	defer f.Close()

	info, err := f.Stat()
	if err != nil {
		log.Println(err)
		http.Error(w, "500 Internal Error: stat() failed.", 500)
		return
	}

    if !info.IsDir() {
        http.ServeContent(w, r, p, info.ModTime(), f)
        return
    }

    entries, err := f.ReadDir(0)
    if err != nil {
		log.Println(err)
        http.Error(w, "500 Internal Error: ReadDir() failed.", 500)
        return
    }

    type Item struct {
        Name  string
        IsDir bool
    }
    var items []Item

    for _, entry := range entries {
        name := entry.Name()
        if name == "." || name == ".." {
            continue
        }
        if entry.IsDir() {
            name += "/"
        }
        items = append(items, Item { Name: name, IsDir: entry.IsDir() })
    }

    sort.Slice(items, func(i, j int) bool {
        if items[i].IsDir != items[j].IsDir {
            return items[i].IsDir
        }
        return items[i].Name < items[j].Name
    })

    templ, err := template.New("templ").Parse(DIR_TEMPL)
	if err != nil {
        log.Println(err)
		http.Error(w, "500 Internal Error: Error while generating directory listing.", 500)
		return
	}

    err = templ.Execute(w, items)
	if err != nil {
		log.Println(err)
	}
}

const DIR_TEMPL = `<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
<title>Index of...</title>
</head>
<body>
<form method="post" enctype="multipart/form-data">
<input type="file" name="files" id="files" multiple>
<input type="submit" name="submit" value="Submit">
</form>
<progress value="0" max="100"></progress>
<pre>
<a href="..">..</a>
{{ range . -}}
<a href="{{ .Name }}">{{ .Name }}</a>
{{ end -}}
</pre>
<script>
const progress = document.querySelector("progress");
const form = document.querySelector("form");

form.addEventListener("submit", function(e) {
    e.preventDefault();

    const xhr = new XMLHttpRequest();
    xhr.upload.addEventListener("progress", function(e) {
        progress.value = Math.floor((e.loaded / e.total) * 100);
    });
    xhr.addEventListener("load", function(e) {
        window.location.reload();
    });
    xhr.open("POST", "/", true);

    const data = new FormData(form);
    xhr.send(data);
});
</script>
</body>
</html>
`
